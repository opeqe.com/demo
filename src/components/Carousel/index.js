import React from 'react';

import styles from './Carousel.module.scss';

import Container from '@material-ui/core/Container';
import Radio from '@material-ui/core/Radio';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

import CarouselItem from '../CarouselItem';

export default class Carousel extends React.Component {

  render () {
    return (
      <Box display="flex"
           flexDirection="column"
           className={styles.carouselRoot}>

        <Box flexGrow={1} display="flex" flexDirection="column">
          <CarouselItem />
        </Box>

        <Grid container justify="center">
          <Grid item>
            <Radio/>
          </Grid>
          <Grid item>
            <Radio/>
          </Grid>
          <Grid item>
            <Radio/>
          </Grid>
        </Grid>

      </Box>
    );
  }

}
