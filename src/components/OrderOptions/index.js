import React from 'react';

import styles from './OrderOptions.module.scss';

import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

export default class OrderOptions extends React.Component {

  render () {
    return (
      <>
        <div className={styles.wrapper}></div>

        <Box className={styles.stickyBar}>

          <Container className={styles.optionsContainer}>
            <span>
              ASAP
              <br/>
              Address?
            </span>

            <Button>Change</Button>
          </Container>

        </Box>
      </>
    );
  }

}
