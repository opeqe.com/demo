import React from 'react';

import styles from './Menu.module.scss';

import Box from '@material-ui/core/Box';

import OrderOptions from '../OrderOptions';
import Footer from '../Footer';
import Slider from '../Slider';

export default class Menu extends React.Component {

  render () {
    return (
      <>
        <OrderOptions />

        <div className={styles.appContent}>

          <Box className={styles.sliderContainer}>
            <Slider />

            <Slider />

            <Slider />

            <Slider />
          </Box>

          <Footer />

        </div>
      </>
    );
  }

}
