import React from 'react';

import styles from './HScroll.module.scss';

import Box from '@material-ui/core/Box';

export default class HScroll extends React.Component {

  render () {
    return (
      <div className={styles.hscrollRoot}>

        <div className={styles.cursorWrapper}>
          <div className={styles.cursor} style={{ left: 0 }}></div>
        </div>

      </div>
    );
  }

}
