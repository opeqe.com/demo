import React from 'react';

import styles from './SliderItem.module.scss';

import Box from '@material-ui/core/Box';

export default class SliderItem extends React.Component {

  render () {
    return (
      <Box className={styles.sliderItemRoot}>
        SliderItem
      </Box>
    );
  }

}
