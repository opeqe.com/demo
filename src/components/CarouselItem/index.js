import React from 'react';

import styles from './CarouselItem.module.scss';

import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';

export default class CarouselItem extends React.Component {

  render () {
    return (
      <Container className={styles.carouselItemRoot}>

        <Box flexGrow={1}>
          <strong>Title</strong>
          <p>Foo</p>
        </Box>

        <Box>Bar</Box>

      </Container>
    );
  }

}
