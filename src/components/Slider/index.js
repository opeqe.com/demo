import React from 'react';

import styles from './Slider.module.scss';

import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';

import HScroll from '../HScroll';
import SliderItem from '../SliderItem';

export default class Slider extends React.Component {

  render () {
    return (
      <Container className={styles.sliderRoot}>
        <Box>
          <strong>Slider Title</strong>
        </Box>

        <HScroll />

        <Box display="flex"
             flexDirection="row"
             className={styles.itemContainer}>

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

          <SliderItem />

        </Box>
      </Container>
    );
  }

};
