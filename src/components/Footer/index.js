import React from 'react';

import styles from './Footer.module.scss';

import Box from '@material-ui/core/Box';

export default class Footer extends React.Component {

  render () {
    return (
      <Box className={styles.footerRoot}>
        <span style={{color:'white'}}>Footer</span>
      </Box>
    );
  }

}
