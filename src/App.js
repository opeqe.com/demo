import React from 'react';
import logo from './logo.svg';
import styles from './App.module.scss';

import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';

import Carousel from './components/Carousel';
import Footer from './components/Footer';
import Menu from './components/Menu';

function App() {
  return (
    <>
      <AppBar position="fixed" elevation="0" className={styles.mainAppBar}>
        <Toolbar className={styles.mainToolbar}></Toolbar>
      </AppBar>

      <Carousel />

      <Menu />
    </>
  );
}

export default App;
